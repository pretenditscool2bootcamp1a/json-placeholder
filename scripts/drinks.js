document.onload = init();
let drinks = ["Martini", "Margarita,"]
function init() {
  const ptrselect = document.getElementById("selectDrink");
  setupselect(ptrselect);
  ptrselect.onchange = tblupdate;
}

function setupselect(select1) {
  let theOption = new Option("Select One", "");
  select1.appendChild(theOption);
  let unsortedemployeenames = getunsortedemployeenames();
  console.log(unsortedemployeenames);
  let sortedempoyeenames = "";

  sortedempoyeenames = unsortedemployeenames.sort();

  for (let i = 0; i < sortedempoyeenames.length; i++) {
    let theOption = new Option(sortedempoyeenames[i]);

    select1.appendChild(theOption);
  }
}
function cleartable(tbl1) {
  console.log(tbl1);
  tbl1.innerHTML = "";
}

function getunsortedemployeenames(inp1) {
  let retempnames = "";
  for (let i = 0; i < employees.length; i++) {
    retempnames = retempnames + "," + employees[i].name;
  }
  retempnames = retempnames.substring(1);
  // console.log ("unsorted employeenames " + retempnames ) ;
  return retempnames.split(",");
}

function tblupdate() {
  const select1 = document.getElementById("selectEmployee");

  const tbl1 = document.getElementById("employeesTblBody");
  // console.log (tbl1) ;
  cleartable(tbl1);
  // console.log("tblupdate post cleartable ");
  if (select1.selectedIndex == 0) {
    // console.log("bp in the selected index 0 of selection");
    alert("please select an actual value from this drop down");
    // console.log("They need to select an actual value or we won't do anything. currindex = " + select1.selectedIndex);

  }
  else {
    // console.log("in tblupdate about to call filltable function");
    filltable(select1.selectedIndex, tbl1);
  }
}

function filltable(category1, tbl1) {
  const keys = Object.getOwnPropertyNames(employees[category1]);

  for (const [key, value] of Object.entries(employees[category1])) {
    if (`${key}` == "projectsAssignedTo") {
      let numofprojs = employees[category1].projectsAssignedTo.length;
      buildtablerow(tbl1, "Number of assigned projects", numofprojs, true, employees[category1].projectsAssignedTo);
    }
    else {
      buildtablerow(tbl1, `${key}`, `${value}`, false, "");
    }
  }
}

function buildtablerow(tbl1, fieldname, fieldvalue, clickable1, projs1) {
  // console.log("in buildtablerow ");
  let trObject = tbl1.insertRow(-1);
  trObject.scope = "col";
  let cell1 = trObject.insertCell(0);
  cell1.innerHTML = "<b>" + fieldname + "</b>";
  let cell2 = trObject.insertCell(1);
  cell2.innerHTML = fieldvalue;
  if (clickable1) {
    cell2.style = "background-color: coral" ;
    cell2.onclick = function () { showprojects(tbl1 , projs1) ; cell2.style = "background-color: white" ; ; cell2.onclick = "" ;}
  }
}

function showprojects(tbl1, inpprojs) {
  console.log(inpprojs);
  for (let i = 0; i < inpprojs.length; i++) {
    for (const [key, value] of Object.entries(inpprojs[i])){
      // console.log ("key " + `${key}` + " value "  + `${value}`) ;
      buildtablerow(tbl1, `${key}`, `${value}`, false, "");
    }
  }
} 