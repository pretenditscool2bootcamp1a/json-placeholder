window.onload = function () {
  const btn = document.getElementById("mybutton");
  btn.onclick = onClickButton;
};

function onClickButton() {
  let element = document.getElementById("messagebody");
  let id = document.getElementById("userid").value;
  let url = "https://jsonplaceholder.typicode.com/todos/" + id;
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      let message = "<b>To Do:</b> " + data.title + " <br><b>Completed ?</b> " + data.completed;
      element.innerHTML = message;
    });
}
