window.onload = function () {

    const tbl1 = document.getElementById("mytable");
    let url = "https://jsonplaceholder.typicode.com/users";
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            for (let i = 0; i < data.length; i++) {
                let trObject = tbl1.insertRow(-1);
                trObject.scope = "col";
                let cell1 = trObject.insertCell(0);
                cell1.innerHTML = data[i].name;
                let cell2 = trObject.insertCell(1);
                cell2.innerHTML = data[i].username;
                let cell3 = trObject.insertCell(2);
                cell3.innerHTML = data[i].email;
                let cell4 = trObject.insertCell(3);
                cell4.innerHTML = data[i].phone;
                let cell5 = trObject.insertCell(4);
                cell5.innerHTML = data[i].website;
                let cell6 = trObject.insertCell(5);
                cell6.innerHTML = data[i].company.name;
            }
        });
};
